package az.atl.product.model.mapper;

import az.atl.product.ProductApplication;
import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.model.dto.ProductDto;

import java.util.List;

public enum ProductMapper  {

    PRODUCT_MAPPER;

    public static List<ProductDto> buildDtoToList(List<ProductEntity> entityList){
        return entityList.stream().map(ProductMapper::buildDto).toList();
    }

    public static ProductEntity buildEntity(ProductDto productDto){
        return ProductEntity.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .price(productDto.getPrice())
                .count(productDto.getCount())
                .build();
    }

    public static ProductDto buildDto(ProductEntity entity){
        return ProductDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .price(entity.getPrice())
                .count(entity.getCount())
                .build();
    }
}
