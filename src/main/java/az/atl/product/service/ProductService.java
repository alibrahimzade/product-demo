package az.atl.product.service;

import az.atl.product.ProductApplication;
import az.atl.product.model.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllProducts();
    void createProduct(ProductDto productDto);

    ProductDto getProductById(Long id);

}
