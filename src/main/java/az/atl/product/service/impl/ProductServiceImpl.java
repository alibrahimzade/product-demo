package az.atl.product.service.impl;

import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.dao.repository.ProductRepo;
import az.atl.product.model.dto.ProductDto;
import az.atl.product.model.mapper.ProductMapper;
import az.atl.product.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static az.atl.product.model.mapper.ProductMapper.*;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService, CommandLineRunner {
    private final ProductRepo productRepo;
    @Override
    public List<ProductDto> getAllProducts() {
        List<ProductEntity> productList = productRepo.findAll();
        return buildDtoToList(productList);
    }

    @Override
    public void createProduct(ProductDto productDto) {
        ProductEntity product = buildEntity(productDto);
        productRepo.save(product);
    }

    @Override
    public ProductDto getProductById(Long id) {
        var productEntity = productRepo.findById(id);
        return productEntity.map(ProductMapper::buildDto).orElse(null);
    }

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 5; i++) {
            var product = ProductEntity.builder()
                    .name("Product"+i)
                    .price(new BigDecimal(10).add(new BigDecimal(i)))
                    .count(i).build();
                productRepo.save(product);
        }

    }
}
