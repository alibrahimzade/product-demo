package az.atl.product.controller;

import az.atl.product.model.dto.ProductDto;
import az.atl.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/allProducts")
    public List<ProductDto> getAllProducts(){
        return productService.getAllProducts();
    }

    @GetMapping("/byId")
    public ProductDto getProductById(@RequestParam("id") Long id){
        return productService.getProductById(id);
    }

    @PostMapping("/create")
    public void createProduct(@RequestBody ProductDto productDto){
        productService.createProduct(productDto);
    }
}
